class MyQueue(object):
    def __init__(self, maximum):
        self.maximum = maximum
        self.lista = [0] * maximum
        self.front = None
        self.rear = None

    def enqueue(self, val):
        return None

    def print(self):
        print(self.lista)

    def dequeue(self):
        return None

if __name__ == "__main__":
    stos = MyQueue(5)
    stos.print()
    stos.enqueue(3)
    stos.enqueue(4)
    stos.enqueue(9)

    stos.print()
    print("After deleting")
    stos.dequeue()
    stos.print()