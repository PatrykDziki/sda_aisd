import math


class BasicAlgorithms:
    @staticmethod
    def prime(val):
        ret_val = None
        print(ret_val)
        return ret_val

    @staticmethod
    def find_next_square(val):
        return -1


if __name__ == "__main__":
    sda_basic = BasicAlgorithms()

    assert sda_basic.prime(10) is False, "Prime is not working well yet!"
    assert sda_basic.prime(97) is True, "Prime is not working well yet!"
    print("looks like prime is working. Let's see what whit a nextsquare...")

    assert sda_basic.find_next_square(121) == 144, "NextSquare is not working well yet!"
    assert sda_basic.find_next_square(625) == 676, "NextSquare is not working well yet!"
    assert sda_basic.find_next_square(114) == -1, "NextSquare is not working well yet!"

    print("looks like all tests are working")
